<html>
<head>
  <title>Results - CVSin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="Un site de comparaison et d'analyse de CVE simplfié pour optimiser le travail d'analyste." />
  <meta name="keywords" content="cve, security, vulnerability, security vulnerability, exploit, cvss, information security, vulnerability database" />
  <link rel="author" content="Benoît Breul" />

  <!--Zone decorum-->
  <link rel="stylesheet" type="text/css" href="./css/cvesin.css" />

  <script language="JavaScript">
  <!--
  //PLF-/
  function menuderoulant(cadre,selection){
  eval(cadre+".location='"+selection.options[selection.selectedIndex].value+"'");
  }
  //-->
  </script>

<body>

  <?php
  require_once "./phpfunc/functions.php";
  $search = $_GET['search'];
  $limit = $_GET['limit'];
  ?>

  <div id="hautpage">
    <div id="hautpage_gauche">
      <a href="./" title="Home"><img src="./pictures/indexLOGO.png" alt="cvesin" border="0"/></a>
    </div>
    <div id="hautpage_droite">
      <form id="cveSearch" action="search.php" method="get">
        <input id="searchbox" type="text" name="search">
        <input id="limit" name="limit" type="hidden" value="20">
        <input type="submit" name="submit" value="Search">
      </form>
      <i>Exemple: CVE-2020-0001</i>
    </div>
  </div>

  <table id="corpus">
    <tbody>
      <tr>
        <td id="barhaut" colspan="2">
          <div style="float:left;">
            <a class="login" href="/login.php" title="Login">Login</a>
          &nbsp;
            <a class="rss" href="https://rss.breul.fr/" title="RSS">RSS</a>
          </div>
          <div style="float:right;">
            References:&nbsp;
            <a class="veille" href="https://www.ssi.gouv.fr/" title="ANSSI">ANSSI</a>
            &nbsp;
            <a class="veille" href="https://www.cvedetails.com/" title="CVE Details">CVE Details</a>
            &nbsp;
            <a class="veille" href="https://nvd.nist.gov/" title="NVD Nist">NVD Nist</a>
            &nbsp;
            <a class="veille" href="https://cve.mitre.org/" title="Mitre">Mitre</a>
            &nbsp;
            <a class="veille" href="https://www.exploit-db.com/" title="ExploitDB">ExploitDB</a>
          </div>
        </td>
      </tr>
      <tr>
        <td id="menu" valign="top">
          lorum
        </td>
        <td id="intern" valign="top" align="left">
          <div id="container">
            <h1>Résultat pour "<?php echo $search ?>"</h1>
            <div class="sousMenu">
              <div style="float:left">
                Sort résultats:
              </div>
              <div style="float:right">
                <div class="dropdown">
                  <button class="dropbtn"><?php echo $limit ?></button>
                  <div class="dropdown-content">
                  <a href="search.php?search=<?php echo $search ?>&limit=20&submit=Search">20</a>
                  <a href="search.php?search=<?php echo $search ?>&limit=50&submit=Search">50</a>
                  <a href="search.php?search=<?php echo $search ?>&limit=100&submit=Search">100</a>
                  <a href="search.php?search=<?php echo $search ?>&limit=200&submit=Search">200</a>
                  </div>
                </div>
              </div>
              <div style="display:block;">
                <a href="javascript:sertabletotsv('vulnslisttable',true,false,'downloadoverlaydivtxtarea')" title="View results in copy-paste ready tab seperated values format">Copy Results</a>
                &nbsp;
                <a href="javascript:sertabletotsv('vulnslisttable',true,true,null)" title="Chrome and Firefox only- Download results in tab seperated values format">Download Results</a>
              </div>
            </div>
          </div>
          <div id="searchResults">
              <?php
              searchCVE($search,$limit);
              ?>
          </div>
        </td>
      </tr>

    </tbody>
  </table>

  </div>
</body>
</html>
