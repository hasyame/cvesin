<html>
<head>
  <title>CVSin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="Un site de comparaison et d'analyse de CVE simplfié pour optimiser le travail d'analyste." />
  <meta name="keywords" content="cve, security, vulnerability, security vulnerability, exploit, cvss, information security, vulnerability database" />
  <link rel="author" content="Benoît Breul" />
  
  <!--Zone decorum-->
  <link rel="stylesheet" type="text/css" href="./css/cvesin.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>

<body>
  <div id="hautpage">
    <div id="hautpage_gauche">
      <a href="./" title="Home"><img src="./pictures/indexLOGO.png" alt="cvesin" border="0"/></a>
    </div>
    <div id="hautpage_droite">
      <form id="cveSearch" action="search.php" method="get">
        <input id="searchbox" type="text" name="search">
        <input id="limit" name="limit" type="hidden" value="20">
        <input type="submit" name="submit" value="Search">
      </form>
      <i>Exemple: CVE-2020-0001</i>
    </div>
  </div>

  <table id="corpus">
    <tbody>
      <tr>
        <td id="barhaut" colspan="2">
          <div style="float:left;">
            <a class="login" href="/login.php" title="Login">Login</a>
          &nbsp;
            <a class="rss" href="https://rss.breul.fr/" title="RSS">RSS</a>
          </div>
          <div style="float:right;">
            References:&nbsp;
            <a class="veille" href="https://www.ssi.gouv.fr/" title="ANSSI">ANSSI</a>
            &nbsp;
            <a class="Veille" href="https://vigilance.fr/?langue=1" title="Vigilance">Vigilance</a>
            &nbsp;
            <a class="veille" href="https://www.cvedetails.com/" title="CVE Details">CVE Details</a>
            &nbsp;
            <a class="veille" href="https://nvd.nist.gov/" title="NVD Nist">NVD</a>
            &nbsp;
            <a class="veille" href="https://cve.mitre.org/" title="Mitre">Mitre</a>
            &nbsp;
            <a class="veille" href="https://www.exploit-db.com/" title="ExploitDB">ExploitDB</a>
          </div>
        </td>
      </tr>
      <tr>
        <td id="menu" valign="top">
          lorum
        </td>
        <td id="intern" valign="top" align="left">
          ipsum
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
