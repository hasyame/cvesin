CREATE TABLE CVE (
  dataType CHAR(3),
  id VARCHAR(16),
  dateAdd datetime default now(),
  descpb VARCHAR(32),
  description TEXT,
  accessVector VARCHAR(8),
  accessComplexity VARCHAR(8),
  authentication VARCHAR(8),
  confidentialityImpact VARCHAR(8),
  integrityImpact VARCHAR(8),
  availabilityImpact VARCHAR(8),
  baseScore FLOAT(3),
  severity VARCHAR(8),
  exploitabilityScore FLOAT(3),
  impactScore FLOAT(3),
  obtainAllPrivilege VARCHAR(8),
  obtainUserPrivilege VARCHAR(8),
  obtainOtherPrivilege VARCHAR(8),
  userInteractionRequired VARCHAR(8),
  publishedDate VARCHAR(24),
  lastModifiedDate VARCHAR(24)
);

CREATE TABLE cpeCVE (
  id VARCHAR(16),
  cpe VARCHAR(256)
);

CREATE TABLE refCVE (
  id VARCHAR(16),
  urldata TEXT,
  refdata TEXT,
  descname TEXT
);
